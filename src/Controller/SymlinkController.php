<?php

namespace Drupal\symlink\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\node\NodeInterface;

/**
 * Class SymlinkController.
 *
 * @package Drupal\symlink\Controller
 */
class SymlinkController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Method for redirecting to the page for creating the symlink.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *    Returns a redirect response for showing the symlink node add page.
   */
  public function addSymlink(NodeInterface $node) {
    return $this->redirect('node.add', ['node_type' => 'symlink'], [
      'absolute' => TRUE,
      'query' => array(
        'symlink' => "node/{$node->id()}",
      ),
    ]);
  }

}
